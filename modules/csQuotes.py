# csQuotes.py
# I know I probably missed a lot, anyone can add more to it

import random
import re

# Metadata

NAME = 'csQuotes'
ENABLE = True
TYPE = 'command'
PATTERN = '^!csQuotes$'

USAGE = '''Usage: !csQuotes
Displays a quote from a list of computer science quotes from this year
'''

# Constants

QUOTES = (
    "Topical Guide Objective",
    "Hey guys stop using sudo. Bottom Text.",
    "Are you playing bejeweled?",
    "Ferpa",
    "You need to fix that -Ramzi",
    "Fork yourself",
    "Bravo Zulu",
    ''' Advantages and Disadvantages of a Linked List:

    Advantages:
    1) The number of nodes in a list is not fixed and can grow and shrink on
    demand. Any application which has to deal with an unknown number of objects
    should need to use a linked list.
    2) Easier to implement within mathematical model for data types than arrays.

    Disadvantages:
    1) Linked Lists do not allow direct access to the individual elements. If you
    want to access a particular item then you have to start at the head and
    follow the references until you get to that item.
    2) A linked list uses more memory compare with an array, because a register
         must be allocated for each pointer.''',
    "Virtual Office Hours (Meme review?)",
    "What are the three tenets of teh Unix Philosophy?",
    "Cadence Certified",
    "Hoop flow this Thursday at 4",
    "You must maintain the integrity of the data structure",


)




# Command
def command(bot, nick, message, channel, question=None):
        response = random.choice(QUOTES)
        bot.send_response(response, nick, channel)

# Register
def register(bot):
        return (
                        (PATTERN, command),
        )
                        
# vim: set sts=4 sw=4 ts=8 expandtab ft=python:



